# Ali - Animerad CSS theme

## Skapare

Jag heter Alireza Mousavi kallas också för Ali. Jag är 19 år gammal 
och går på Hässleholms tekniska skola. Jag är intresserad av 
webbutveckling och läser sista året på teknikprogrammet med 
inriktning informations- och medieteknik.

## Om themat

Det som är speciellt med det här temat är att jag har använt 
CSS-animation. Jag tycker det är kul med animering och det var först 
av min IT-lärare som jag fick veta att CSS har en sådan funktion. Jag 
läste om den på några olika webbsidor såsom ”W3school” och 
”MDN Web Docs” och fick lära mig lite på det sättet. Jag har 
också använt CSS-gradient med fyra olika färger. Bakgrunden för det 
här temat är nämligen en kombination av CSS-animation och 
CSS-linear-gradient. ”Media Query” har också använts. Vid olika 
skärmstorlekar ändras också bakgrundsfärgen. Dessutom när sidan 
öppnas i mobilen d.v.s. enhet med skärmstorlek som har en bredd 
lägre än 550px visas allt på sidan i mitten. Detta för att 
information ska synas bättre. 
