# A Green style

**by:** ZaDoS

This theme is green. From 2017 Palettes on
[w3schools](https://www.w3schools.com/colors/colors_palettes.asp)

WE will use the following colors:

- HEX: #3e4444
- HEX: #82b74b
- HEX: #405d27
- HEX: #c1946a