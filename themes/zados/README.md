# ZaDoS 

## Skapare
Jag heter Noah Pernetun och är född den 1 augusti 2003. Jag bor i Hässleholm och går på Hässleholms tekniska skola på teknikprogrammet med inriktning IT. På min fritid kollar jag på filmer och serier och spelar Leauge of Legends. Jag tycker att teknik är häftigt och är intresserad av hur teknik fungerar och hur olika tekniker fungerar i samspel med varandra.

## Om themat

Temat använder sig av färgerna blå, gul och lite rött. Dessa färgerna används då Sveriges flagga är blå och gul. Röd används då Skånes flagga är röd och gul. Temat består av ett sidhuvud, en beskrivning, listor och en sidfot. Sidhuvudet innehåller rubriken och knappen som används för att byta tema. Beskrivningen är har en vit bakgrund med rundade hörn. När fönstrets bredd är 577 pixlar eller mindre får beskrivningen en ram som har samma färg som sidhuvudet och blir en del av sidhuvudet. Det som gör temat unikt är att list elementen i lektionsplaneringssidan är block med bilder och animeras för att se mer 3D ut. Listorna som inte ligger på lektionsplaneringssidan använder sig av list-style-image för att använda stjärnor istället för punkter. Sidfoten innehåller information om copyrighten på sidan. När skärmstorleken ändras, förändras olika delar av sidan, till exempel sidhuvudets höjd eller perspektivet (perspective) på listan. 
Temat använer sig av bilder från pexels.com, unsplash.com och pixabay.com.



Alla bilder får användas för komersiellt bruk.
https://www.pexels.com/photo/gray-laptop-computer-showing-html-codes-in-shallow-focus-photography-160107/	av Negative Space
https://unsplash.com/photos/FO7JIlwjOtU		av Alexandre Debiève
https://pixabay.com/illustrations/artificial-intelligence-brain-think-3382507/		av Gerd Altmann
https://unsplash.com/photos/1IW4HQuauSU		av Pankaj Patel
https://unsplash.com/photos/1IW4HQuauSU		av Pankaj Patel
https://unsplash.com/photos/M5tzZtFCOfs		av Taylor Vick
https://www.pexels.com/photo/black-background-with-text-overlay-screengrab-270404/		av Pixabay