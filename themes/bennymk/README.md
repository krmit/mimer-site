# bennymk

## Skapare

Beskrivning: Jag är en kille med rötter ifrån Kina men jag har bott 
i Sverige under hela mitt liv. Jag gillar att spela datorspel under 
fritiden, främst FPS men även lite MOBA. Jag bor i Hässleholm och 
har alltid varit intresserad av datorer och det som har med det att 
göra.

## Om themat

Mitt tema är ett tema med inspiration ifrån en så kallad “dark 
mode” hemsida, alltså att hemsidan ska vara designad för att vara 
rätt så mörk men inte svart. Det är på grund av att jag tycker det 
blir skönare att titta på en mörk skärm när jag sitter i ett 
mörkt rum vilket jag gör rätt ofta.
