# A blue style

**by:** krm

This theme is blue. From Sky Palettes on
[w3schools](https://www.w3schools.com/colors/colors_palettes.asp)

WE will use the following colors:

- HEX: #cfe0e8
- HEX: #b7d7e8
- HEX: #87bdd8
- HEX: #daebe8
- HEX: #8d9db6 (Denna färg används inte)
