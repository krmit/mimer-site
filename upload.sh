#!/usr/bin/env sh

FLAGS="-avr -P"
INCLUDE="--include=\"**.html\" --include=\"**.css\" --include=\"*/\""
INCLUDE_IMAGE="--include=\"**.png\"" 

echo "Web resources";
#rsync $FLAGS  $INCLUDE $INCLUDE_IMAGE --exclude="*" ./web/v/* krm@htsit.se:~/www/v
rsync -avr -P  --include="**.html" --include="**.css" --include="*/" --include="**.png" --exclude=* ./web/v/* krm@htsit.se:~/www/v

echo "Content for academy";
rsync -avr -P  --include="**.html" --include="**.css" --include="*/" --include="**.png" --exclude=* ./web/a/* krm@htsit.se:~/www/a

echo "Content for private project under codword ko";
#rsync $FLAGS  $INCLUDE $INCLUDE_IMAGE --exclude="*" ./web/ko/* krm@htsit.se:~/www/ko
rsync -avr -P  --include="**.html" --include="**.css" --include="*/" --include="**.png" --exclude=* ./web/ko/* krm@htsit.se:~/www/ko

echo "Index files";
mtext -wr ./web/t/index.md;
mv ./web/t/index.md.html ./web/t/index.html;
rsync ./web/t/index.html krm@htsit.se:~/www/t/index.html
