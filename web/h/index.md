# Hall of fame
__
author: krm
created: 2023-11-30
__

Några intresnata elevarbeten. Läsåretsarbeten har prioritet, men även 
äldre arbeten kan visas upp.

# Webbutveckling

- 🔗 [Sanrio sido](https://htsit.se/u/moon/testsida/)
- 🔗 [Ninjago sida](https://htsit.se/u/avrige/testsida/)
- 🔗 [Julkort 2022](https://htsit.se/j/xmasCard/2022-it-to-troll/)
