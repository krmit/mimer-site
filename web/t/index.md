# Lista över resurser hösten 2023
__
author: krm
created: 2023-11-13
__

Observera att denna lista kommer att göras om under jullovet, men allt material kommer att vara samma.

# Komma igång

För alla kurser.

- 🔗 [Mimer template](https://gitlab.com/krmit/mimer-template)
- 🔗 [Installera node](https://htsit.se/t/install-node.sv.md.html)
- 🔗 [Installera och komma igång med git](https://htsit.se/t/install-git.sv.md.html)
- 🔗 [Klona ett akiv med git](https://htsit.se/t/clone-git-repository.sv.md.html)
- 🔗 [Starta utveckla](https://htsit.se/t/start-development.md.html)

# Gemensamt

# Webbserverprogrammering 1

- 🔗 [Skapa en ny webbserver](https://htsit.se/t/create-a-project-for-webserver.sv.md.html)
- 🔗 [Skapa till en statisk webbmapp](https://htsit.se/t/web-map-for-static-content.sv.md.html)
- 🔗 [Exempel: get](https://htsit.se/e/get/examples.sv.md.html)
- 🔗 [Exempel: url variabler](https://htsit.se/e/url-parameters/examples.sv.md.html)
- 🔗 [Exempel: kakor](https://htsit.se/e/cookies/examples.sv.md.html)
- 🔗 [Hämta data från ett formulär](https://htsit.se/t/get-information-through-a-form.sv.md.html) 
- 🔗 [Template med en websida](https://htsit.se/t/using-mustasch-for-pages.sv.md.html)

# Programmering 2

- 🔗 [Skapa ett terminal program](https://htsit.se/t/create-a-project-for-termninal.sv.md.html)
- 🔗 [Hitta ett packet](https://htsit.se/t/find-a-usefull-package.sv.md.html)
- 🔗 [Tester med Mocha](https://htsit.se/t/create-unit-test.md.html)
- 🔗 [Dokumentation med tsDoc](https://htsit.se/t/documenting-code.sv.md.html)
- 🔗 [Exempel: Test](https://htsit.se/e/test/examples.sv.md.html)
- 🔗 [Exempel: Documentation](https://htsit.se/e/doc/examples.sv.md.html)
- 🔗 [Exempel: Packetet Color](https://htsit.se/e/color/examples.sv.md.html)
- 🔗 [Exempel: prompt](https://htsit.se/e/prompt//examples.sv.md.html)
- 🔗 [Exempel: fs](../e/fs/examples.sv.md.html)
- 🔗 [Exempel: fetch](../e/fetch/examples.sv.md.html)
- 🔗 [Demo: AsciiSign](https://gitlab.com/krmit/ascii-sign.git)

# Webbutveckling 1

- 🔗 [Skapa ett webbsida](https://htsit.se/t/create-a-web-page.sv.md.html)
- 🔗 [Skapa en css fil](https://htsit.se/t/create-a-css-file.sv.md.html)
- 🔗 [Skapa ett formulär](https://htsit.se/t/create-a-web-form.sv.md.html)
- 🔗 [Exempel: Formulär]((https://htsit.se/e/forms/examples.sv.md.html)
- 🔗 [Exempel: Layoute](https://htsit.se/e/layoute/examples.sv.md.html)
