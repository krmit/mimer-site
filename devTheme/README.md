# Demo of themes

This folder is created to make it simple to develop themes for
*mimer-site* projekt.

In *docs-for-css.md* it is documentation for all class and id name
used for themes.

In folder *themes* is all themes for the site. The current theme is in
"default" folder. Make a symbolic link from the theme folder that
interest you to the default folder. You could also develop direct in
the default folder, but be careful so you do not delete it by mistake.

In folder *demo* there are some demo pages on the site. All of the is
listed in the index page.

 
In the *css* folder there is css file imported by themes or demos
in order to get them to work. Especially *base-for-color-theme.css* is
interesting because it is the base for all simple color themes.
