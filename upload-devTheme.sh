#!/usr/bin/env bash

zip -r devTheme.zip devTheme;
scp -r devTheme.zip krm@htsit.se:~/www/f/;
rm devTheme.zip;
