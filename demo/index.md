# Demo för CSS Thema

Sidorna nedan är exempel på hur ett tema för *mimer-site* kommer att se 
ut. Öppna dem i en webbläsaren för att se hur sidan ser ut med ett 
tema. Temat som kommer användas är det som finns i default mappen 
under *theme* mapen.

Observera att länkar inte kommer att fungera på exempel sidorna.

## Kurser

  * [Index över kurser](./course/course-index.html)
  * [Huvudsida för varje kurs](./course/course-main.html)
  * [Moment](./course/part.html)
  * [Lektionsbeskrivning](./course/lession.html)

## Akademin

  * [Index över ämnen i akdemin](./academy/academy-index.html)
  * [Ämnen](./academy/subject.html)
  * [Kapitel](./academy/chapter.html)
  * [Avsnitt](./academy/section.html)
  * [Kort](./academy/card.html)
  * [Lista över uppgifter](./academy/exercises_list.html)
  * [Övning](./academy/exercises.html) Observera att själva editorn inte komemr att synas.

## Gemensamma resurser

  * [Error 404](./common/error-404.html)
