"use strict";
import m from "mithril";
import { View } from "./view";

export class Load extends View implements m.Component {
  kind: string;

  constructor(vnode: m.Vnode) {
    super(vnode);
    this.kind = "a load component for when we are waiting.";
  }

  view() {
    return m(
      "div",
      { id: "load", class: "container" },
      m("h1", { id: "load", class: "title" }, "Loading ...")
    );
  }
}
