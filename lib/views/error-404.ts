"use strict";
import m from "mithril";
import {Error} from "./error";

export class Error404 extends Error implements m.Component {

  constructor(vnode: m.Vnode) {
    super(vnode);
    this.error = 404;
    this.description = "Could not find a page for that path.";
  }
}
