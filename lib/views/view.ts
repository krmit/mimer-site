"use strict";
import m from "mithril";

export class View {
  kind: string;

  constructor(vnode: m.Vnode) {
    this.kind = "a component.";
  }

  oncreate() {
    console.log(`Created ${this.kind}`);
  }
}
