"use strict";
import m from "mithril";
import {View} from "./view"
import {Load} from "./load"
import {PathModels as PM} from "../models/pathModels"
import {Label} from "../components/label";

export class Home extends View implements m.Component {
  kind: string;
  name: string;
  path: string;
  prefix: string;
  model: PM;
  label: Label;
  lang: {title: string, description:string} = {  title: "", description: ""};

  constructor(vnode: m.Vnode, model:PM, label:Label, name:string, prefix:string) {
    super(vnode);
    this.kind = "a home component for the main page.";
    this.path="";
    this.model = model;
    this.label = label;
    this.name = name;
    this.prefix=prefix;
  }

  oninit() {
    this.model.load();
  }

  view() {
    if (this.model.data) {
      const content = this.model.data;
      let label = this.label;
      let lang = this.lang;
      let prefix = this.prefix;

      return m("div", { class: this.name+" home container" }, [
        m("h1", { class: "title" }, lang.title),
        m(
          "p",
          { class: "desdescription" },
          lang.description
        ),
        m("ul", { class: "list" }, 
        Object.keys(content.subtopics).sort().map(function(path: string) {
          const course: any = content.subtopics[path];
                return m(
                  "li",
                  { class: "item" },
                  m(
                    m.route.Link,
                    { href: prefix+"/" +course.name+"/",
              class: "link"	
            },
                    m(label, course)
                  )
                );
              })
        )
      ]);
    } else {
      return m(Load);
    }
  }
}
