"use strict";
import m from "mithril";
import {View} from "./view";

export class Error extends View implements m.Component {
  kind: string;
  error: number;
  description: string;

  constructor(vnode: m.Vnode) {
    super(vnode);
    this.kind = "a error need to be shown.";
    this.error = 0;
    this.description="";
  }

  view() {
    return m("div", {class: "error container" }, [
      m(
        "h1",
        { class: "error title" },
        "Error " + this.error
      ),
      m(
        "p",
        {class: "errors description" },
        this.description
      )
    ]);
  }
}
