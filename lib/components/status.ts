"use strict";
import m from "mithril";
import { Component } from "./component";

type StatusType = "draft"|"alpha"|"beta"|"rc"|"ok";

interface StatusAttrs {
	status: StatusType;
}

export class Status extends Component implements m.ClassComponent<StatusAttrs> {

  view({attrs: {status}}: m.CVnode<StatusAttrs>) {   
    return m(
      "span",
      {class: "status-symbol symbol" }, StatusToSymbol(status));
  }
}

function StatusToSymbol(status:StatusType ) {
	let result:string;  
	switch (status) {
	  case "draft":
		result = "❓";
	  break;
	  case "alpha":
		result = "📝";
	  break;
	  case "beta":
		result = "🥉";
	  break;
	  case "rc":
		  result = "🥈";
	  break;
	  case "ok":
		  result = "🥇";
	  break;
	break;
	  default:
		  result = "❌";
	  }
	  return result;
  }