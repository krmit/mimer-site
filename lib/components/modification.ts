"use strict";
import m from "mithril";
import { Component } from "./component";

interface ModificationAttrs {
	data: any;
}

export class Modification extends Component implements m.ClassComponent<ModificationAttrs> {

  view(vnode: m.CVnode<ModificationAttrs>) {
	let {attrs: {data}} = vnode; 
	let m_list = [];

// Modifications

	const mod_class = [];

	if(data.tags) {
	if(data.tags.includes("repetion")) {
	  mod_class.push("repetion");
	  m_list.push(m("span",{class: "icon"},"♻️"));
	}
	
	if(data.tags.includes("bonus")) {
	  mod_class.push("bonus");
	  m_list.push(m("span",{class: "icon"},"➕"));
	}
	
    if(data.tags.includes("update")) {
	  mod_class.push("update");
	  m_list.push(m("span",{class: "icon"},"🔔"));
	}
	
	if(data.tags.includes("defect")) {
	  mod_class.push("defect");
	  m_list.push(m("span",{class: "icon"},"⚠️"));
	}
	
	if(data.tags.includes("important")) {
	  mod_class.push("important");
	  m_list.push(m("span",{class: "icon"},"🔥"));
	}
	
	if(data.tags.includes("asap")) {
	  mod_class.push("asap");
	  m_list.push(m("span",{class: "icon"},"⏰"));
	}
	
    if(data.tags.includes("unsupported"))  {
	  mod_class.push("unsupported");
	  m_list.push(m("span",{class: "icon"},"🔕"));
	}
	
	if(data.tags.includes("extra")) {
	  mod_class.push("extra");
	  m_list.push(m("span",{class: "icon"},"💎"));
	}
}
	if(mod_class.length === 0){
	 return m("span",vnode.children);	
	} else {
	   	mod_class.push("modification");
    return m(
      "span",{class: "modification"}, [
      " ",
      m("span",{class: "icons" },m_list),
       " ",
      m("span",{class: mod_class.join(" ")},vnode.children)
      ]
    );
}
  }
}
