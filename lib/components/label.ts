"use strict";
import m from "mithril";
import {Component} from "./component";

interface CardAttrs {
	title: string;
}

export class Label extends Component implements m.ClassComponent<CardAttrs> {
    classes: string[];
    
    constructor(classes:string[]) {
        super();
       this.classes = classes;
    }

    view({attrs: {title}}: m.CVnode<CardAttrs>) {
     let classes_text = this.classes.join("");
      return m(
        "span",
        {class: classes_text + " label" },
        m("strong", {}, title)
      );
    }
  }
