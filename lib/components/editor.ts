"use strict";
import m from "mithril";
import {Component} from "./component";
import {EditorState} from "@codemirror/state"
import {EditorView, keymap} from "@codemirror/view"
import {defaultKeymap} from "@codemirror/commands"
import {defaultHighlightStyle} from "@codemirror/highlight"
import {javascript} from "@codemirror/lang-javascript"
import {oneDark} from "@codemirror/theme-one-dark"
import {lineNumbers} from "@codemirror/gutter";

interface EditorAttrs {
	code: string;
}

export class Editor extends Component implements m.ClassComponent<EditorAttrs> {
    editor?:EditorView
    state?:EditorState
    code="";
    
    constructor() {
        super();
    }

    oncreate(vnode:any) {
      const id:any = vnode.dom;
      console.log("Create editor");
      this.state = EditorState.create({
        doc: this.code,
        extensions: [keymap.of(defaultKeymap),
          lineNumbers(),
          javascript(),
          defaultHighlightStyle
        ]
      })
      
      this.editor = new EditorView({
        state: this.state,
        parent: id,
      })
    }

    getState() {
      console.log("BOOM");
      return this.state;
    }

    view({attrs: {code}}: m.CVnode<EditorAttrs>) {
      if(this.editor && this.code !== code) {
        const editor = this.editor;
        editor.dispatch({
          changes: {from: 0, to: editor.state.doc.length, insert: code}
        })
         m.redraw(); 
      }
      this.code = code;
      return m("div",{ id: "Editor"});
    }
  }
