"use strict";
import m from "mithril";
import { Component } from "./component";
import { Modification } from "./modification";

interface ElementAttrs {
	data: any;
}

interface ElementLink {
	title:string;
	url:string
}

interface ElementResourse {
	title?:string;
	link: ElementLink;
}

export class Element extends Component implements m.ClassComponent<ElementAttrs> {

  view({attrs: {data}}: m.CVnode<ElementAttrs>) {
	let component:any;
    if(data.tags !== undefined) {
	if(data.tags.includes("section")) {
	  component = createResourse("📜", data, "section");
	} else if(data.tags.includes("chapter")) {
	  component = createResourse("📘", data, "chapter");
	} else if(data.tags.includes("document")) {
	  component = createResourse("📄", data, "document");
	} else if(data.tags.includes("💬")) {
	  component = createResourse("💬", data, "terminology");
	} else if(data.tags.includes("liveEditor")) {
	  component = createResourse("🖋", data, "liveEditor");
	} else if(data.tags.includes("examples")) {
	  component = createResourse("⚙️", data, "examples");
	} else if(data.tags.includes("question")) {
		component = createResourse("🤔", data, "question");
	} else if(data.tags.includes("wikipedia")) {
		component = createResourse("🌐", data, "wikipedia");
	} else if(data.tags.includes("video")) {
		component = createResourse("🎥", data, "video");
	} else if(data.tags.includes("link")) {
	  component = createResourse("🔗", data, "link");
	} else if(data.tags.includes("git")) {
	  component = createResourse("📂", data, "git");
	} else if(data.tags.includes("submission")) {
	  component = createResourse("📨", data, "submission");
	} else if(data.tags.includes("exam")) {
	  component = createResourse("📝", data, "exam");
	} else if(data.tags.includes("question")) {
	  component = createNotification("❓", data.text, "question");
	} else if(data.tags.includes("rule")) {
	  component = createNotification("❗", data.text, "rule");
	} else if(data.tags.includes("todo")) {
	  component = createNotification("🚩", data.text, "todo");
	} else if(data.tags.includes("comment")) {
	  component = createNotification("", data.text, "comment");
	} else if(data.tags.includes("black")) {
	  component = createTrack("⬛", data.excersises, "track-black");
	} else if(data.tags.includes("blue")) {
	  component = createTrack("🔷", data.excersises, "track-blue");
	} else if(data.tags.includes("green")) {
	  component = createTrack("💚", data.excersises, "track-green");
	} else {
		component = m("span",{class: "no-data"},"???");
	}
    } else {
		component = m("span",{class: "note"},data.title);
	}
	   	
    return m(
      "span",
      {class: "lession-label label" }, m(Modification, {data:data},
      component
    ));
  }
}

function createResourse(icon:string,element:ElementResourse, className:string) {
	  return m("span", {}, [
	  m("a",{class: className+" resourse link", href: element.link.url},
	  " ",
	  m("span",{class: "icon"},icon),
	  " ",
	  element.link.title),
	  element.title?m("em",{},element.title):""
	  ]);
	}

function createNotification(icon:string,title:string, className:string) {
	  return m("span",{class: className+" note"},
	  " ",
	  m("span",{class: "icon"},icon),
	  " ",
	  title);
	}

function createTrack(icon:string,data:any, className:string) {
	  let component_list = [];
	  
	  for(const excersise of data) {
		  if(excersise.type==="jul") {
			component_list.push(createExcersiseBuletin(excersise.type,excersise.year));  
		  } else if(excersise.type==="po") {
			component_list.push(createExcersisePO(excersise.type,excersise.year));  
		  } else if(excersise.level) {
			component_list.push(createExcersisePlauground(excersise.level,Number(excersise.placing), excersise));  
		  } else {
			component_list.push(m("span",{class: "no-data"},"???"));
		  }
		  component_list.push(", ");
	  }
	   component_list.pop();
	  
	  return m("span",{class: className+" track"},
	  " ",
	  m("span",{class: "icon"},icon),
	  " ",
	  m("span",{class: "excersise"},component_list));
	}

function createExcersisePlauground(level:string,placing:number, data:any) {
	return m("a", {href: `https://htsit.se/aa/#!/p/${level}${placing}`, class: "link", target: "_blank"}, m(Modification, {data:data}, level+String(placing)));
}

function createExcersiseBuletin(type:string,year:number) {
	return m("a", {href: `https://htsit.se/j/${year}/`, class: "link", target: "_blank"}, m("span", {}, "J"+String(year)));
}

function createExcersisePO(type:string,year:number) {
	return m("a", {href: `https://www.progolymp.se/`, class: "link", target: "_blank"}, m("span", {}, "PO"+String(year)));
}
