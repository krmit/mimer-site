"use strict";
import { Model } from "./model";

export class Lang extends Model {
  static msg: any;
  static lang: string;

  constructor() {
    super();
  }

  static load(lang: string = "sv") {
    Lang.lang = lang;
    const lang_path = "lang/"+lang + ".json";
    return super.load(lang_path).then((result: any) => {
      Lang.msg = result;
    });
  }
}
