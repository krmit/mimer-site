"use strict";
import { Model } from "./model";

export class PathModels extends Model {
  static content: any;
  static label: any;
  name: string;
  path: string[];

  constructor(...path:string[]) {
    super();
    this.path = path;
    this.name = path.filter((e)=>{return e !== ""}).join("/");
  }

  get data():any {
      return PathModels.content[this.name];
  }

  load() {
    if(PathModels.content === undefined) {
      PathModels.content = {};
    }
    return Model.load(this.name).then((result: any) => {
      PathModels.label = "Path Model";
      PathModels.content[this.name] = result;
    });
  }
}
