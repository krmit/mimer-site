"use strict";
import m from "mithril";

export class Model {
  static data: any = {};
  static dataURL = "/";

  constructor() {}

  static load(path: string) {
    const data_path = Model.dataURL + path;

    if (Model.data[path]) {
      return Promise.resolve(Model.data[path]);
    } else {
      return m
        .request({
          method: "GET",
          url: data_path
        })
        .then(function(data: any) {
          Model.data[path] = data;
          console.log("Loaded data for " + path);
          return data;
        });
    }
  }
}
