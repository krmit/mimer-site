"use strict";
import { Model } from "./model";

export class Courses extends Model {
  static content: any;
  static label: string;

  constructor() {
    super();
  }

  static load(name: string) {
    const courses_path = "courses/" + name + "/data.sv.json";
    if(this.content === undefined) {
      this.content = {};
    }
    return super.load(courses_path).then((result: any) => {
      Courses.label = name;
      Courses.content[name] = result;
    });
  }
}
