export const style:any = {};

style.setup = function (root:any) {
  style.root = root;
  
    if (localStorage && localStorage.getItem("style")) {
      style.name = localStorage.getItem("style");
    } else {
      style.name = "blank";
    }

    // From https://stackoverflow.com/a/41356010/1070904 by Camilo
    style.link = document.getElementById("style");
    style.link.href = style.root + "themes/" + style.name + "/main.css";
    console.log("Set style to: " + style.name);
    
    return style.link.href;
};

style.set = function (name:string) {
  if (localStorage) {
    localStorage.setItem("style", name);
  }
  style.name = name;
  style.link.href = style.root + "themes/" + style.name + "/main.css";
};
