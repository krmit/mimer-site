const style = {};
export default style;

style.setup = function (root) {
  style.root = root;

  return () => {
    if (localStorage && localStorage.getItem("style")) {
      style.name = localStorage.getItem("style");
    } else {
      style.name = "red";
    }

    // From https://stackoverflow.com/a/41356010/1070904 by Camilo
    style.link = document.getElementById("style");
    style.link.href = style.root + "themes/" + style.name + "/main.css";
  };
};

style.set = function (name) {
  if (localStorage) {
    localStorage.setItem("style", name);
  }
  style.name = name;
  style.link.href = style.root + "themes/" + style.name + "/main.css";
};
